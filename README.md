Codestat
===

Small and simple program to get certain stats from a plaintext file. 

## Installation

```$ sudo ./install.sh```

## Usage 


```$ codestat file```
