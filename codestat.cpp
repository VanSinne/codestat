#include <iostream>
#include <sstream> // stringstream
#include <fstream>
#include <string>
#include <unistd.h> // sleep() in unix
#include <cctype>
using namespace std;

// Function for counting spaces (words)
int count_words(string simp){
    int numspaces = 0;
    char nextChar;
    // checks each character in the string
    for (int i=0; i<int(simp.length()); i++)
    {
    	nextChar = simp.at(i); // gets a character
    	if (isspace(simp[i]))
    		numspaces++;
    }
    return numspaces+1;
}

int count_lines(string simp){
return 0;    // Needs code to count lines in string (fstr, defined below)
}

int count_characters(string simp){
return 0;    // Needs code to count characters in string (fstr, defined below)
}

int main (int argc, char *argv[]){

    // Define file as the first program parameter
    ifstream bfile (argv[1]);
    if(bfile.is_open()) {
        // Get the file contents to a variable
        stringstream bvar;
        bvar << bfile.rdbuf();
        string fstr = bvar.str();
        // fstr is now a string containing input file contents.



        cout << "Your file contains " << count_words(fstr) << " words."<< endl; 
    }
}
