#!/bin/bash
# Check user privileges
if [ "$EUID" -ne 0 ]; then 
    echo "This program needs root privileges."
    echo "Run as root or with 'sudo ./install.sh'."
    exit
fi

function compile {
    g++ codestat.cpp -o codestat
}
function move {
    mv codestat /usr/bin/
}
function uninstall {
    rm -rf /usr/bin/codestat
}


# Check if file already exist in path
file=codestat
if [[ $(type -P "$file") ]]; then
    echo "Binary from previous installation found."
    echo "Do you wish to upgrade or uninstall?"
    echo "1. Upgrade (default)"
    echo "2. Uninstall (Completely remove program)"
    printf 'Alternative: '
    read -r method
    # if method !=1or0, requery...
else
    printf 'This script will compile codestat and put it into /usr/bin/. \nContinue? (y/n): '
    read -r confirm
    # if confirm != y or n, requery
fi

if  [[ $confirm == "y" ]] || [[ $confirm == "Y" ]] || [[ $method = "1" ]]; then
    compile
    move
    if [[ $(type -P "$file") ]]; then
        echo "Installation successful."
    else
        echo "Installation failed."
    fi

else if [[ $method == "2" ]]; then
    uninstall
        if [[ $(type -P "$file") ]]; then
            echo "Uninstallation failed."
        else
            echo "Uninstallation successful."
        fi

else
    echo "Aborting." # If answer is not y or n (replace with requery above)
    exit
fi
fi
